# Prepoznavanje lica - Python i OpenCV

#  Članak o prepoznavanju lica (https://www.superdatascience.com/opencv-face-detection/).
#  Kao osnova korišćen je open source projekat (https://github.com/informramiz/opencv-face-recognition-python)


import cv2  # Glavni modul, koristimo ga za detekciju i prepoznavanje lica
import os  # Modul koji koristimo da čitamo i modifikujemo fajlove i datoteke
import numpy as np  # Modul koji koristimo da konvertujemo python liste u nympy arrays, jer openCV radi sa njima
import glob  # Modul za pathnames i operacije nad njima, konkretno koristimo za putanje prilikom kopiranje slika
import shutil  # Modul za operacije visokog levela nad fajlovima, konkretno za kopiranje slika i brisanje datoteke sa sadržajem

# Što više slika kao uzorak to će bolji rezultati biti. Takođe je bitno da se dodaju slike osobe koja se smeje, je ozbiljna, sa bradom, bez brade, sa naočarima, bez naočara itd.
# 
# Trening podaci se nalaze u folderu training-data. Za svakog subjekta imamo po jedan folder sa slikama. Struktura foldera:
# Folderi subjekata počinju slovom "s".
# 
# ```
# training-data
# |-------------- s<indeks0>-<ime0>
# |               |-- 1.jpg
# |               |-- ...
# |               |-- 12.jpg
# |-------------- s<indeks1>-<ime1>
# |               |-- 1.jpg
# |               |-- ...
# |               |-- 12.jpg
# ```
#

# OpenCV kao labele koristi integere, zato je napravljena lista subjects. Npr. subjects[0] = "Loki", subjects[1] = "DJ Ani"
subjects = [None] * 100

def add_subject(name, folder):  # Funkcija koja dodaje lice u sistem. Ulazni paremtri su ime i folder sa slikama lica
    if name in subjects:  # Ako je subjekat ne ubace se opet
        return False

    subjects.append(name)
    index = len(subjects) - 1
    destination = "./training-data/s" + str(index) + "-" + name  # kreira se ime foldera za subjekta
    os.mkdir(destination)
    copy_images(folder, destination)
    return True


def remove_subject(name):  # Funkcija koja briše lice iz sistema. Ulazni parametar ime subjekta
    if name not in subjects:  # Ako subjekat ne postoji ne nastavlja se
        return False
    subjects.remove(name)
    dirs = os.listdir("training-data")
    for dir_name in dirs:  # Traži se folder koji pripada subjektu

        # Ignorišu se folderi koji nisu imenovani po konvenciji
        if not dir_name.startswith("s"):
            continue

        my_dir_name = dir_name.replace("s", "")
        char_index = my_dir_name.find("-")
        label = int(my_dir_name[0:char_index])
        subject_name = dir_name[char_index + 2:]
        if subject_name == name:
            delete_dir("./training-data/" + dir_name)  # Kad se nađe traženi folder, obriše se
            return True
    return False


def delete_dir(dir_name):
    shutil.rmtree(dir_name)


def copy_images(source_dir, destination_dir):
    for jpgfile in glob.iglob(os.path.join(source_dir, "*.jpg")):
        shutil.copy(jpgfile, destination_dir)


# OpenCV face recognizer koristi dva vektora za rad. Jedan vektor sadrži lica osoba, a drugi labele kako bi ih označio.
# 
# Na primer 2 osobe sa 2 slike lica
# 
# ```
# PERSON-1    PERSON-2   
# 
# img1        img1         
# img2        img2
# ```
# 
# Priprema podataka rezultira sledećim rezultatom
# 
# ```
# FACES                        LABELS
# 
# person1_img1_face              1
# person1_img2_face              1
# person2_img1_face              2
# person2_img2_face              2
# ```
# 
# 
# Priprema podataka ima sledeće korake.
# 
# 1. Pročitaju se imena svih foldera u folderu training-data.
# 2. Za svakog subjekta ekstraktujemo labelu (indeks) iz imena foldera
# 3. Pročitamo svaku sliku iz foldera i izvršimo detekciju lica sa svake slike
# 4. Svako lice se doda u vektoru lica sa odgovarajućom labelom
#


def detect_face(img):  # Funkcija za detektovanje lica na slici
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Konvertuje test sliku u grayscale jer openCV face detector radi sa takvim slikama

    face_cascade = cv2.CascadeClassifier('opencv-files/lbpcascade_frontalface.xml')  # Učitavanje openCV face detector-a

    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5)  # Detektovanje multiscale, vraća listu lica

    if len(faces) == 0:
        return None, None

    (x, y, w, h) = faces[0]  # Pretpostavka da se na slici nalazi samo jedno lice. Učitavamo rect gde je lice

    return gray[y:y+w, x:x+h], faces[0]  # Vraća deo slike gde je lice


def prepare_training_data(data_folder_path):  # Ova funkcija čita sve slike namenjene za treniranje sistema. Detektuje lica sa slike i vraća 2 liste. Jedna lista lica, druga lista labela za svako lice

    dirs = os.listdir(data_folder_path)

    faces = []
    labels = []

    for dir_name in dirs:
        if not dir_name.startswith("s"):
            continue

        my_dir_name = dir_name.replace("s", "")
        char_index = my_dir_name.find("-")
        label = int(my_dir_name[0:char_index])

        subject_dir_path = data_folder_path + "/" + dir_name  # Gradi putanju do slika subjekta
        subject_images_names = os.listdir(subject_dir_path)  # Učitavanje svih imena slika subjekta

        for image_name in subject_images_names:  # Prolaz kroz sve slike, detekcija lica i dodavanje lica u vektor lica

            if image_name.startswith("."):  # Ignorisanje sakrivenih, sistemskih fajlova
                continue

            image_path = subject_dir_path + "/" + image_name
            image = cv2.imread(image_path)
            cv2.imshow("Training on image...", cv2.resize(image, (400, 500)))  # Prikazivanje slike na ekranu
            cv2.waitKey(100)  # Vremen za gašenje prozora u ms

            face, rect = detect_face(image)

            if face is not None:  # Ukoliko je prepoznato lice na slici
                faces.append(face)  # Dodavanje lica u vektor lica
                labels.append(label)  # Dodavanje labele u vektor labela
            
    cv2.destroyAllWindows()
    cv2.waitKey(1)
    cv2.destroyAllWindows()
    
    return faces, labels


def prepare_training():  # Vrši se priprema podataka za trening. Vraća 2 liste. Lista faca i labela koje odgovaraju licima
    print("Preparing data...")
    faces, labels = prepare_training_data("training-data")
    print("Data prepared")
    print("Total faces: ", len(faces))
    print("Total labels: ", len(labels))
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    cv2.waitKey(1)
    cv2.destroyAllWindows()
    return faces, labels


face_recognizer = cv2.face.LBPHFaceRecognizer_create()  # Kreira LBPH face recognizer


def begin_training(faces, labels):  # Nakon što su trening podaci pripremljeni, počinje trening
    face_recognizer.train(faces, np.array(labels))


def draw_rectangle(img, rect):  # Crta pravougaonik na slici
    (x, y, w, h) = rect
    cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)


def draw_text(img, text, x, y):  # Ispisuje tekst na slici od x,y pozicije
    cv2.putText(img, text, (x, y), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 255, 0), 2)


def predict(test_img):  #  Ova funkcija prepoznaje osobu sa slike, obeležava lice i ispisuje ime osobe za koju veruje da se nalazi na slici
    img = test_img.copy()  # Kopiramo sliku kako ne bi menjali original
    face, rect = detect_face(img)
    label, confidence = face_recognizer.predict(face)  # Vraća labelu po ocenama funkcije za prepoznavanje lica. Na koga najviše liči osoba sa test slike i udaljenost od poklapanja
    disMax = 250.0
    simMax = 100.0
    if confidence >= disMax:
        sim = str(0)
    else:
        sim = simMax - simMax / disMax * confidence
    label_text = subjects[label]
    percentage = str(round(sim, 2)) + "%"

    draw_rectangle(img, rect)  # Obeležavanje lica na slici
    draw_text(img, label_text, rect[0], rect[1]-5)  # Crtanje ime osobe za koju se smatra da se nalazi na slici
    
    return img, label_text, percentage


def load_image(image_path):  # Učitavanje slike sa date putanje
    return cv2.imread(image_path)


def display_image(image, title):  # Prikazivanje slike u prozoru veličine 400x500
    cv2.imshow(title, cv2.resize(image, (400, 500)))
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def initial_scan(folder):  # Učitavanje subjekata iz foldera training-data
    global subjects
    print("Initial scan on startup in progress...")
    dirs = os.listdir(folder)
    max_label = -1
    for dir_name in dirs:

        # Ignorisanje imena foldera koji nisu po konvenciji
        if not dir_name.startswith("s"):
            continue

        my_dir_name = dir_name.replace("s", "")
        char_index = my_dir_name.find("-")
        label = int(my_dir_name[0:char_index])
        if label > max_label:
            max_label = label
        subject_name = dir_name[char_index + 2:]  # Ekstraktovanje imena subjekta iz imena foldera
        subjects.insert(label, subject_name)
        print("Added subject: " + subject_name)
    subjects = subjects[:max_label+1]
    print(subjects)


def int_try_parse(value):  # Sprečava pucanje programa ako korisnik ne unese integer, a trebalo bi
    try:
        return int(value), True
    except ValueError:
        return value, False


def strip_quotes(string):  # Skida navodnike na početku i kraju stringa, za potrebe putanje fajla
    if string.startswith("\"") and string.endswith("\""):
        string = string[1:-1]
    return string
