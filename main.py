import facial_recognition  # Učitavanje biblioteke za prepoznavanje lica


def new_line():  # Štampa novu liniju na konzolid
    print("")


def main():  # Glavna funkcija
    facial_recognition.initial_scan("training-data")  # Inicijalno skeniranje na pokretanje sistema
    print("*NOTE: System works with .jpg images only*")
    main_loop()


def main_loop():  # Glavna petlja koja prikazuje meni, uzima komande od korisnika i izvršava ih
    while True:
        new_line()
        print_menu()
        command = take_input()
        do_command(command)


def print_menu():  # Štampa meni sa mogućim komandama
    print("What would you like to do: ")
    print("1) Add new face")
    print("2) Remove face")
    print("3) Train system")
    print("4) Scan image to identify face")
    print("5) Exit")


def take_input():  # Uzima input od korisnika
    new_line()
    command = input("Your command: ")
    return command


def do_command(command):  # Izvršava izabranu komandu
    command, converted = facial_recognition.int_try_parse(command)
    if not converted:
        print("### Command must be numeric value.")
        return

    if command == 1:
        add_face()
    if command == 2:
        remove_face()
    if command == 3:
        train()
    if command == 4:
        scan_face()
    if command == 5:
        quit()


def add_face():  # Funkcija za dodavanje novog lica u sistemu
    name = input("Name of person to add: ")  # Primer: Blade
    folder = input("Folder path of facial images: ")  # Pimer: C:\Users\Me\Downloads\Blade
    folder = facial_recognition.strip_quotes(folder)

    if not facial_recognition.os.path.isdir(folder):
        print("### Entered path is not a folder. Try again!")
        return

    success = facial_recognition.add_subject(name, folder)  # Poziv funkcije iz biblioteke koja dodaje lice kao subjekat
    if success:
        print("### Person is added.")
    else:
        print("### Person with same id already exists.")


def remove_face():  # Funkcija za brisanje lica iz sistema
    name = input("Name of person to remove: ")
    success = facial_recognition.remove_subject(name)  # Poziv funkcije iz biblioteke koja briše subjekat iz sistema
    if success:
        print("### Person is removed from system.")
    else:
        print("### There is no person with specified id registered in system.")


def train():  # Sistem trenira sa podacima iz training-data foldera. Funkcija koja se mora pozvati pre bilo kakvog testiranja.
    faces, labels = facial_recognition.prepare_training()
    facial_recognition.begin_training(faces, labels)


def scan_face():  # Funkcija koja skenira sliku test lica i vraća ime (i procenat sigurnosti) za koga smatra da se nalazi na slici iz sistema
    image_path = input("Image path: ")  # Primer: E:\Slike\slika.jpg
    image_path = facial_recognition.strip_quotes(image_path)
    if not facial_recognition.os.path.isfile(image_path):
        print("### File does not exists. Try again")
        return

    image = facial_recognition.load_image(image_path)  # Učitavanje slike sa zadate putanje
    if image is None:
        print("### Test image could not be loaded!")
        return

    predicted, name, percentage = facial_recognition.predict(image)  # Upoređuje histograme test slike i već obrađenih slika
    output = name + " (" + percentage + ")"
    print("On that picture is " + output)
    facial_recognition.display_image(predicted, output)  # Prikazuje rezultat kao sliku


main()

